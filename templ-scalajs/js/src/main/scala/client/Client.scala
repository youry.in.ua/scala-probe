package client

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel, JSGlobal}
import org.scalajs.dom.document
import org.scalajs.dom.html

@JSExportTopLevel("Client")
object Client {

  @JSExport
  def main(id: String): Unit = {
    println(s"Start $id")
    val div = document.getElementById(id).asInstanceOf[html.Div]
    div.textContent = "Hello!"
  }

}
