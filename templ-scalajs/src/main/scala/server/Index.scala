package server

import scalatags.Text.all._

object Index {
  //val boot =
  //  "console.log('onLoad');"
  val skeleton =
    html(
      head(
        meta(charset:="utf-8"),
        script(src:="client-fastopt.js"),
      ),
      body(
        div(id:="hello"),
        script("document.addEventListener('DOMContentLoaded', function() {Client.main('hello')})"),
      )
    )
}
