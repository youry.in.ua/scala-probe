name := "gomoku-akka"

version := "1.0"

scalaVersion := "2.12.6"

lazy val akkaVersion = "2.5.16"

//libraryDependencies += "org.scala-lang" % "scala-swing" % "2.10+"
libraryDependencies += "org.scala-lang.modules" %% "scala-swing" % "2.0.0-M2"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)
