package gomoku


case class Pack(
  status: String,
  game: Array[Tuple2[Int, Int]]
)

case class Step(
  x: Int,
  y: Int,
)

case class Net(
  allSlots : Array[Slot],
  allPoints : Array[Point],
  union : Array[Union],
  steps : Array[Step],
)

case class Point(
  x : Int,
  y : Int,
  sp : Int,
)

case class Slot(
  scpX : Int,
  scpY : Int,
  d : Int,
  rs : Int,
  ss : Int,
)

case class Union(
  point : Point,
  slot : Slot,
)

object Game  {


  def calculate(pack: Pack): Pack = {

    // isPointInSlot :: Point -> Slot -> Bool
    val isPointInSlot: (Point, Slot) => Boolean = (point, slot) => {
      val ds = slot.d
      val xs = slot.scpX
      val ys = slot.scpY
      val xp = point.x
      val yp = point.y
      val dx = xp - xs
      val dy = yp - ys

      (point, slot) match {
          case _ if ds == 0 && dx == 0 && Math.abs(dy) < 3 => true
          case _ if ds == 1 && dy == 0 && Math.abs(dx) < 3 => true
          case _ if ds == 2 && dx == dy && Math.abs(dx) < 3 => true
          case _ if ds == 3 && dx == -dy && Math.abs(dx) < 3 => true
          case _  => false
      }
    }

    // applyStep :: Net -> Step -> Net
    val applyStep: (Net, Step) => Net = (net, step) => {

      // nextAllPoints :: Net -> Step -> [Point]
      val nextAllPoints: (Net, Step) => Array[Point] = (net, step) => {

        val points = net.allPoints
        val c = 1 + net.steps.length % 2

        (for {p <- points} yield(if (p.x == step.x && p.y == step.y) Point(step.x, step.y, c) else p)).toArray
      }

      // nextAllSlots :: Net -> Step -> [Slot]
      val nextAllSlots: (Net, Step) => Array[Slot] = (net, step) => {

        val slots = net.allSlots
        val c = 1 + net.steps.length % 2

        val nextSlot: (Slot, Int, Int) => Slot = (slot, x, y) => {
          if (isPointInSlot(Point(x, y, 0), slot)) {
            val st = slot.ss
            val scpx = slot.scpX
            val scpy = slot.scpY
            val sd = slot.d
            val srs = slot.rs

            st match {
              case _ if st == 0 => Slot(scpx, scpy, sd, 1, c)
              case _ if st == c => Slot(scpx, scpy, sd, srs + 1, st)
              case _ if st != c && st < 3 => Slot(scpx, scpy, sd, -1, 3)
              case _ => slot
            }
          } else {
            slot
          }
        }

        slots.map(nextSlot(_, step.x, step.y))

      }

      val nextPoints = nextAllPoints(net, step)
      val nextSlots = nextAllSlots(net, step)

      val newNet = Net(
        nextSlots,
        nextPoints,
        (for {p <- nextPoints; s <- nextSlots; if isPointInSlot(p, s)} yield(Union(p, s))).toArray,
        net.steps :+ step,
      )

      //logger.info(s"applyStep step: (${step.x},${step.y}), points: ${newNet.allPoints.length}, slots: ${newNet.allSlots.length}, union: ${newNet.union.length}")
      //dumpNet(newNet, s"${step.x}-${step.y}.net")
      newNet
    }

    //newGame :: Net -> Pack
    val newGame: Net => Pack =  net =>  {

      // checkOver :: Net -> Bool
      val checkOver: Net => Boolean = net => {

        // activeSlots :: Net -> [[Slot]]
        val activeSlots: Net => (Array[Slot], Array[Slot], Array[Slot]) = net => {
          val alls = net.allSlots

          val slots0 = alls.filter(_.ss == 0)
          val slots1 = alls.filter(_.ss == 1)
          val slots2 = alls.filter(_.ss == 2)

          (slots0, slots1, slots2)
        }

        // checkWin :: Net -> Bool
        val checkWin: Net => Boolean =  net => {
          val (_, a1, a2) = activeSlots(net)
          a1.filter(_.rs == 5).length > 0 || a2.filter(_.rs == 5).length > 0
        }

        // checkDraw :: Net -> Bool
        val checkDraw: Net => Boolean = net => {
          val (a0, a1, a2) = activeSlots(net)
          a0.length == 0 && a1.length == 0 && a2.length == 0
        }

        checkWin(net) || checkDraw(net)
      }

      // nextNet :: Net -> Step -> Net
      val nextNet: (Net, Step) => Net = (net, step) => applyStep(net, step)

      //logger.info(s"newGame: ${net.allPoints.length}, slots: ${net.allSlots.length}, union: ${net.union.length}")

      // calcStep :: Net -> [Int]
      val calcStep: Net => Step = net => {

        // pick :: [[Int]] -> [Int]
        val pick: Array[Step] => Step =  xs => {
            val rnd = new scala.util.Random
            xs(rnd.nextInt(xs.length))
        }

        // findSlot4 :: Net -> Int -> [Step]
        val findSlot4: (Net, Int) => Array[Step] = (net, c) => {
          //logger.info(s"findSlot4 $c")
          net.union.filter(u => u.slot.ss == c && u.slot.rs == 4 && u.point.sp == 0).map(u => Step(u.point.x, u.point.y))
        }

        // calcPointMaxRate :: Net -> Int -> [Step]
        val calcPointMaxRate: (Net, Int) => Array[Step] = (net, c) => {

          // emptyPoints :: Net -> [Point]
          val emptyPoints: Net => Array[Point] =  net => net.allPoints.filter(p => p.sp == 0)

          // ratePoint :: Net -> Point -> Int -> Int
          val ratePoint: (Net, Point, Int) => Int =  (net, p, c) => {

            val calc: Slot => Int = s => s.ss match {
              case 0 => 1
              case 3 => 0
              case _ => (1 + s.rs) * (1 + s.rs)
            }

            val pointSlots = net.union.filter(u => u.point.x == p.x && u.point.y == p.y).map(u => u.slot)
            val rList = pointSlots.map(calc(_))
            rList.reduceLeft(_ + _)
          }

          //logger.info(s"calcPointMaxRate color: $c")
          //logger.info(s"calcPointMaxRate: ${net.allPoints.length}, slots: ${net.allSlots.length}, union: ${net.union.length}")
          val a = emptyPoints(net).map(p => (ratePoint(net, p, c), p))
          //logger.info(s"calcPointMaxRate emptyPoints: ${net.allPoints.filter(p => p.sp == 0).length}")
          val m = a.map(e => {val (r, _) = e; r} ).reduceLeft(_ max _)
          a.filter(e => {val (r, _) = e; r == m}).map(e => {val (_, p) = e; Step(p.x, p.y)})
        }

        //logger.info(s"calcStep")
        //ogger.info(s"calcStep: ${net.allPoints.length}, slots: ${net.allSlots.length}, union: ${net.union.length}")

        val c = 1 + net.steps.length % 2
        lazy val slot4c = findSlot4(net, c)
        lazy val slot4ac = findSlot4(net, (3 - c))
        //lazy val findX = findPointX(net, c)
        lazy val maxRate = calcPointMaxRate(net, c)

        net match {
          case _ if slot4c.length > 0 => pick(slot4c)
          case _ if slot4ac.length > 0 => pick(slot4ac)
          //case _ if findX.length > 0 => pick(findX)
          case _ if maxRate.length > 0 => pick(maxRate)
          case _ => throw new Exception("Can't calculate next step")
        }
      }

      val nextStep = calcStep(net)

      // fromSteps :: [Step] -> [[Int]]
      val fromSteps: Array[Step] => Array[Tuple2[Int, Int]] = steps => {
        steps.map(s => {(s.x, s.y)})
      }

      net match {
        case _ if checkOver(net) => Pack("over",fromSteps(net.steps))
        case _ if checkOver(nextNet(net, nextStep)) => Pack("over", fromSteps(net.steps :+ nextStep))
        case _ => Pack("play", fromSteps(net.steps :+ nextStep))
      }
    }

    // newNet :: Pack -> Net
    val newNet: Pack => Net = oldGame =>  {

      // initNet :: Net
      val initNet:  Net = {

        // isScpValid :: Int -> Int -> Int -> Bool
        val isScpValid: (Int, Int, Int) => Boolean =  (x, y, d) => (x, y, d) match {
          case _ if d == 0 && y > 1 && y < 13 => true
          case _ if d == 1 && x > 1 && x < 13 => true
          case _ if d == 2 && (x > 1 && y < 13) && (x < 13 && y > 1) => true
          case _ if d == 3 && (x > 1 && y > 1) && (x < 13 && y < 13) => true
          case _   => false
        }

        val range = (0 to 14).toArray
        val fillAllPoints = (for {i <- range; j <- range} yield(Point(i, j, 0))).toArray
        val fillAllSlots = (for {i <- range; j <- range; k <- 0 to 3; if isScpValid(i, j, k)} yield(Slot(i, j, k, 0, 0))).toArray
        val net = Net (
          fillAllSlots,
          fillAllPoints,
          (for {p <- fillAllPoints; s <- fillAllSlots; if isPointInSlot(p, s)} yield(Union(p, s))).toArray,
          Array[Step](),
        )

        //logger.info(s"initNet points: ${net.allPoints.length}, slots: ${net.allSlots.length}, union: ${net.union.length}")
        //dumpNet(net, "0.net")
        net
      }

      // applySteps :: Net -> [Step] -> Net
      val applySteps: (Net, Array[Step]) => Net = (net, game) => {
        game.foldLeft(net){applyStep(_, _)}
      }

      // toSteps :: [[Int]] -> [Step]
      val toSteps: Array[Tuple2[Int, Int]] => Array[Step] = game => {
        game.map(t => { val (x, y) = t;  Step(x, y)})
      }

      applySteps(initNet, toSteps(oldGame.game))
    }

    // calculate :: Pack -> Pack
    val calc: Pack => Pack = oldGame => newGame(newNet(oldGame))

    calc(pack)
  }
/*
  def dumpNet(net: Net, name: String) = {
    import java.io._
    val pw = new PrintWriter(new File(s"/tmp/gomoku/$name"))
    pw.print(net.toJson.prettyPrint)
    pw.close
  }
*/
}
