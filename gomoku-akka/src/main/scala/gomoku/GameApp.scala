package gomoku

import akka.actor.ActorSystem
//import scala.io.StdIn

object GameApp {

  val system = ActorSystem("game-system")

  // Create top level supervisor
  val supervisor = system.actorOf(GameSupervisor.props(), "game-supervisor")

  def main(args: Array[String]): Unit = {

    try {


      //val board = new Board
      //val ui = new UI(board)
      UI.visible = true

      // Exit the system after ENTER is pressed
      //StdIn.readLine()
    } finally {
      println("Game started")
    }
  }

  def quit() {
    try {
      //
      supervisor ! GameSupervisor.Quit
      println("Game finished")
      system.terminate()
      sys.exit(0)
    } finally {
    }
  }

}
