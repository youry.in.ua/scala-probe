package gomoku

import akka.actor.{ Actor, ActorLogging, Props }

object GameSupervisor {

  def props(): Props = Props(new GameSupervisor)

  final case class Quit()
}

class GameSupervisor extends Actor with ActorLogging {
  import GameSupervisor._

  val calculator = context.actorOf(StepCalculator.props(), "step-calculator")

  override def preStart(): Unit = log.info("Game Application started")
  override def postStop(): Unit = log.info("Game Application stopped")

  // No need to handle any messages
  //override def receive = Actor.emptyBehavior

  override def receive: Receive = {
    case StepCalculator.CalculateStep(pack) =>
      calculator ! StepCalculator.CalculateStep(pack)
    case StepCalculator.RespondStep(pack) =>
      Desk.receiveStep(pack)
    case Quit() =>
      context.stop(self)
  }

}
