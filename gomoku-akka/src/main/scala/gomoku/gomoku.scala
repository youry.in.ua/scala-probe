package object gomoku {
  type KSlot = Tuple3[Int,Int,Int]
  type KPoint = Tuple2[Int,Int]
}
