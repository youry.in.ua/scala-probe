package gomoku

import scala.swing._
import scala.swing.event._
import java.awt.{Color,Graphics2D,BasicStroke}
import java.awt.geom._

object Desk extends Component {

  var busy = false
  var auto = false
  var status = "play"

  val d = 31
  val n = 15
  val p = 8
  val py = 27
  val ur = 12

  val l = d * n
  val width = d * n + d + (p + 1) * 2
  val height = d * n + d + (p * 2 + 1 + 35) + (p * 2 + 1 + 18)
  val cx = (0 to 14).map {i => i * d + d + p + 1}
  val cy = (0 to 14).map {i => i * d + d + (p + 1) * 2 + py}

  val mode = "Black"

  val cc = new Color(128,128,128) //"rgba(128,128,128,1)"
  val b = new Color(0,0,0) //"rgba(0,0,0,1)"
  val w = new Color(255,255,255) //"rgba(255,255,255,1)"
  val g = new Color(128,128,128) //"rgba(128,128,128,1)"

  val cb = Array(b, g)
  val cw = Array(g, w)
  val cs = Array(b, w)

  //var player = 1
  var steps = Array((7,7))

  //val board: Board = new Board

  preferredSize = new Dimension(514, 583)

  listenTo(mouse.clicks)
  reactions += {
    case MouseClicked(_, p, _, _, _) => mouseClick(p.x, p.y)
  }

  def mouseClick(x: Int, y: Int) {

    if (!busy) {
      if (status != "over") {
        //println(s"MouseClicked x:$x, y:$y")

        val s = getPoint(x, y)
        //println(s"Point:(${s._1},${s._2})")
        if ((0 to 14).contains(s._1) && (0 to 14).contains(s._2) && steps.filter(e => e._1 == s._1 && e._2 == s._2).length == 0) {
          steps = steps ++ Array(s)
          println(s"User step - n:${steps.length}, x:${s._1}, y:${s._2}, status: ${status}")
          repaint
          requestStep
        }
      }

    }
  }

  def requestStep() {
    if (!busy) {
      if (status == "play") {
        busy = true
        GameApp.supervisor ! StepCalculator.CalculateStep(new Pack(status, steps))
      }
    }
  }

  def receiveStep(pack: Pack) {
    val s = pack.game.last
    println(s"Calculated step - n:${pack.game.length}, x:${s._1}, y:${s._2}, status: ${pack.status}")

    repaint

    status = pack.status
    steps = pack.game
    
    busy = false
    if (auto)
      requestStep
  }

  def autoGame() {
      auto = true
      requestStep
  }

  def getPoint(px: Int, py: Int): (Int, Int) = {
    val x = cx.indexWhere((e) => px < e + d / 2 && px > e - d / 2)
    val y = cy.indexWhere((e) => py < e + d / 2 && py > e - d / 2)

    //println(s"GetPoint: ${x}, ${y}")
    (x, y)
  }

  def redraw(g : Graphics2D)  {
    drawClear(g)
    drawGrid(g)
    drawSteps(g)

  }

  def drawClear(g : Graphics2D) {

  }

  def drawGrid(g : Graphics2D) {
    g.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING,
		       java.awt.RenderingHints.VALUE_ANTIALIAS_ON)

    g.setColor(new Color(16,16,16))
    g.drawRoundRect(0, 0, width, height, ur, ur)

    g.setColor(new Color(64,64,64));
    g.fillRoundRect(0,0, width, height, ur, ur);

    g.setColor(new Color(64,64,64))
    g.drawRect(p + 1, (p + 1) * 2 + py, l + d, l + d)

    g.setColor(new Color(224,224,224));
    g.fillRect(p + 1, (p + 1) * 2 + py, l + d, l + d);

    g.setColor(new Color(4,4,4));
    g.setStroke(new BasicStroke(1.5f))
    val path = new GeneralPath

    (0 to 14).map( i => {
        path.moveTo(d + p + 1, cy(i))
        path.lineTo(l + p + 1, cy(i))
        path.moveTo(cx(i), d + (p + 1) * 2 + py)
        path.lineTo(cx(i), l + (p + 1) * 2 + py)
      }
    )

    g.draw(path)

  }

  def drawSteps(g : Graphics2D) {
    for (i <- 0 to steps.length - 1) {
      val color = i % 2
      val x = steps(i)._1
      val y = steps(i)._2

      g.setColor(cs(color));
      g.fillArc(cx(x) - d/2, cy(y) - d/2, d, d, 0, 360);

      g.setColor(cc)
      g.drawArc(cx(x) - d/2, cy(y) - d/2, d, d, 0, 360)

    }
  }

  override def paintComponent(g : Graphics2D) {
    redraw(g)
  }

}

object UI extends MainFrame {

  //val board: Board = new Board

  private def restrictHeight(s: Component) {
    s.maximumSize = new Dimension(Short.MaxValue, s.preferredSize.height)
  }

  title = "Gomoku Akka"

  //val canvas = new Canvas()
  val newGameButton = Button("New Game") { newGame() }
  val calcStepButton = Button("Step") { nextStep() }
  val autoButton = Button("Auto") { autoGame() }
  val quitButton = Button("Quit") { GameApp.quit() }
  val buttonLine = new BoxPanel(Orientation.Horizontal) {
    contents += newGameButton
    contents += Swing.HStrut(5)
    contents += calcStepButton
    contents += Swing.HStrut(5)
    contents += autoButton
    contents += Swing.HGlue
    contents += quitButton
  }

  // make sure that resizing only changes the TicTacToeDisplay
  restrictHeight(buttonLine)

  contents = new BoxPanel(Orientation.Vertical) {
    contents += Desk
    contents += Swing.VStrut(10)
    contents += buttonLine
    border = Swing.EmptyBorder(10, 10, 10, 10)
  }

  def newGame() {
    println("New Game!")
    Desk.status = "play"
    Desk.steps = Array((7,7))
    Desk.repaint
  }

  def nextStep() {
    Desk.requestStep
  }

  def autoGame() {
    Desk.autoGame
  }
}
