package gomoku

import akka.actor.{ Actor, ActorLogging, Props, ActorRef }

case class AStep(x: Int, y: Int)

object ANet {
  def props(): Props = Props(new ANet())

  final case class ApplyStep(c: Int, step: AStep)
  final case class CheckOver(c: Int)
}

class ANet() extends Actor with ActorLogging {
  import ANet._

  val isScpValid: (Int, Int, Int) => Boolean =  (x, y, d) => (x, y, d) match {
    case _ if d == 0 && y > 1 && y < 13 => true
    case _ if d == 1 && x > 1 && x < 13 => true
    case _ if d == 2 && (x > 1 && y < 13) && (x < 13 && y > 1) => true
    case _ if d == 3 && (x > 1 && y > 1) && (x < 13 && y < 13) => true
    case _   => false
  }

  val range = (0 to 14).toArray

  val allPoints: Map[KPoint, ActorRef] =
    (for {i <- range; j <- range}
      yield((i, j) -> context.actorOf(APoint.props(i, j), s"point-$i-$j")))
        .toMap

  val allSlots: Map[KSlot, ActorRef] =
    (for {i <- range; j <- range; k <- 0 to 3; if isScpValid(i, j, k)}
      yield((i, j, k) -> context.actorOf(ASlot.props(i, j, k), s"slot-$i-$j-$k")))
        .toMap

  val isPointInSlot: (KPoint, KSlot) => Boolean = (point, slot) => {

    val xs = slot._1
    val ys = slot._2
    val ds = slot._3

    val xp = point._1
    val yp = point._2

    val dx = xp - xs
    val dy = yp - ys

    (point, slot) match {
        case _ if ds == 0 && dx == 0 && Math.abs(dy) < 3 => true
        case _ if ds == 1 && dy == 0 && Math.abs(dx) < 3 => true
        case _ if ds == 2 && dx == dy && Math.abs(dx) < 3 => true
        case _ if ds == 3 && dx == -dy && Math.abs(dx) < 3 => true
        case _  => false
    }
  }

  for ((pk, p) <- allPoints; (sk, s) <- allSlots; if isPointInSlot(pk, sk)) {
    p ! APoint.AddSlot(sk, s)
    s ! ASlot.AddPoint(pk, p)
  }

  override def preStart(): Unit = log.info("Net actor started")
  override def postStop(): Unit = log.info("Net actor stopped")

  override def receive: Receive = {
    case ApplyStep(c, step) => {
      allPoints((step.x, step.y)) ! APoint.SetStatus(c)
    }
    case CheckOver(c) => 
  }

}

object APoint {
  def props(x: Int, y: Int): Props = Props(new APoint(x, y))

  final case class SetStatus(c: Int)
  final case class AddSlot(k: KSlot, s: ActorRef)
}

class APoint(val x: Int, val y: Int) extends Actor with ActorLogging {
  import scala.collection.mutable.Map
  import APoint._

  var sp: Int = 0
  val slots = Map[KSlot, ActorRef]()

  //override def preStart(): Unit = log.info("Point actor started")
  //override def postStop(): Unit = log.info("Point actor stopped")

  override def receive: Receive = {
    case AddSlot(k, s) => {
        slots += (k -> s)
    }
    case SetStatus(c) => {
      sp = c
    }
  }

}

object ASlot {
  def props(x: Int, y: Int, k: Int): Props = Props(new ASlot(x, y, k))

  final case class SetStatus(s: Int)
  final case class SetRate(s: Int)
  final case class AddPoint(k: KPoint, p: ActorRef)
}

class ASlot(val x: Int, val y: Int, val d: Int) extends Actor with ActorLogging {
  import scala.collection.mutable.Map
  import ASlot._

  var ss: Int = 0
  var rs: Int = 0
  val points = Map[KPoint, ActorRef]()

  //override def preStart(): Unit = log.info("Slot actor started")
  //override def postStop(): Unit = log.info("Slot actor stopped")

  override def receive: Receive = {
    case AddPoint(k, p) => {
        points += (k -> p)
    }
    case SetStatus(i) => {
      ss = i
    }
    case SetRate(i) => {
      rs = i
    }
  }

}
