package gomoku

import akka.actor.{ Actor, ActorLogging, Props, ActorRef }

object StepCalculator {
  def props(): Props = Props(new StepCalculator())

  final case class CalculateStep(pack: Pack)
  final case class RespondStep(pack: Pack)
}

class StepCalculator() extends Actor with ActorLogging {
  import StepCalculator._

  var supervisor: ActorRef = null

  override def preStart(): Unit = log.info("StepCalculator actor started")
  override def postStop(): Unit = log.info("StepCalculator actor stopped")

  override def receive: Receive = {
    case CalculateStep(pack) => {
      val resp = Game.calculate(pack)
      sender() ! RespondStep(resp)
      //supervisor = sender()
      //calculate(pack)
    }
    case RespondStep(pack) =>
      supervisor ! RespondStep(pack)
  }

  def calculate(pack: Pack) {
    val net = context.actorOf(ANet.props(), "net")
    for ((s, i) <- pack.game.zipWithIndex) {
      net ! ANet.ApplyStep(i % 2, AStep(s._1, s._2))
    }
    net ! ANet.CheckOver(pack.game.length % 2)
  }

}
