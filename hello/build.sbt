ThisBuild / scalaVersion := ScalaConfig.version //"2.12.7"
ThisBuild / organization := BuildConfig.organization //"gitlab.com/youry.in.ua/scala-probe"

lazy val hello = (project in file("."))
  .settings(
    name := BuildConfig.appName, //"Hello",
    LogConfig.logDirKey := LogConfig.logDir,
    // The build info plugin writes these values into a BuildInfo object in the build info package
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion, LogConfig.logDirKey),
    // The logback.groovy config file uses this build info to configure logging, for example
    buildInfoPackage := "appbuildinfo",

    libraryDependencies += "biz.enef" %% "slogging-slf4j" % "0.5.2",
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.8",
    libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.5.19",
    libraryDependencies += "org.codehaus.groovy" % "groovy-all" % "2.4.7",
    libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.7",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.19",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  ).enablePlugins(BuildInfoPlugin)
