/**
  * Configuration for this project.
  */
object BuildConfig {
  /** The applications version. We use semantic versioning */
  val appVersion = "0.0.1"

  /** The name of this application */
  val appName = "Hello"

  val organization = "gitlab.com/youry.in.ua/scala-probe"
}
