package fxprobe


import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.geometry.Insets
import scalafx.scene.text.Text
import scalafx.scene.paint.{Stops, LinearGradient}
import scalafx.scene.effect.DropShadow

object FxProbe extends JFXApp {

  stage = new PrimaryStage {
    title = "ScalaFX Hello World"
    scene = new Scene {
      fill = Black
      content = new HBox {
        padding = Insets(20)
        children = Seq(
          new Text {
            text = "ScalaFx "
            style = "-fx-font-size: 32pt"
            fill = new LinearGradient(
              endX = 0,
              stops = Stops(PaleGreen, SeaGreen))
          },
          new Text {
            text = printRuntimeInfo
            style = "-fx-font-size: 32pt"
            fill = new LinearGradient(
              endX = 0,
              stops = Stops(Cyan, DodgerBlue)
            )
            effect = new DropShadow {
              color = DodgerBlue
              radius = 25
              spread = 0.25
            }
          }
        )
      }
    }
  }

  private def printRuntimeInfo(): String = {
    val runtime = Runtime.getRuntime
    val mb = 1024 * 1024
    val str = s"JVM max heap memory: ${runtime.maxMemory() / mb} MB. Available processors: ${runtime.availableProcessors}"
    
    str
  }


/*

  def main(args: Array[String]): Unit = {

    LoggerConfig.factory = SLF4JLoggerFactory()

    logger.info("Let's start the {} server :-)", BuildInfo.name)
    printRuntimeInfo()

    //implicit val system = ActorSystem("my-system")
    //implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    //implicit val executionContext = system.dispatcher



    val  route: HttpRequest => HttpResponse = {
      case HttpRequest(GET, Uri.Path("/"), _, _, _) =>
        HttpResponse(entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          "<html><body>Hello world!</body></html>"))

      case HttpRequest(GET, Uri.Path("/ping"), _, _, _) =>
        HttpResponse(entity = "PONG!")

      case HttpRequest(GET, Uri.Path("/crash"), _, _, _) =>
        sys.error("BOOM!")

      case r: HttpRequest =>
        r.discardEntityBytes() // important to drain incoming HTTP Entity stream
        HttpResponse(404, entity = "Unknown resource!")
    }

    val bindingFuture = Http().bindAndHandleSync(route, "localhost", 8080)

    logger.info(s"Server online at http://localhost:8080/")

    println("\nPress RETURN to stop...")

    StdIn.readLine()

    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

  }
*/
}
