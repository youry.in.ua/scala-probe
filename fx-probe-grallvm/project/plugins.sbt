// Make information about our build, such as the application name, available in the application.
// We use this, for example, to get the name of the JavaScript file that contains the transpiled Scala of our app.
// https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")
