ThisBuild / scalaVersion := ScalaConfig.version //"2.12.7"
ThisBuild / organization := BuildConfig.organization //"gitlab.com/youry.in.ua/scala-probe"

lazy val hello = (project in file("."))
  .settings(
    name := BuildConfig.appName, //"Hello",
    //
    libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.144-R12",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  )
