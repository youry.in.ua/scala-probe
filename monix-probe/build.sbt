
import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

name := "Monix probe project"

scalaVersion in ThisBuild := "2.12.7"

lazy val root = project.in(file("."))
  .aggregate(probeJS, probeJVM)
  .dependsOn(probeJS, probeJVM)
  .settings(
    publish := {},
    publishLocal := {},
    libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.7",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.19",
    resources in Compile += (fastOptJS in Compile in probeJS).value.data,
    //mainClass in Compile := (mainClass in Compile in templJVM).value,
  )

lazy val probe = crossProject(JSPlatform, JVMPlatform).in(file("."))

  .settings(
    name := "Probe",
    version := "0.1-SNAPSHOT",
    libraryDependencies += "com.lihaoyi" %%% "scalatags" % "0.6.7",
    libraryDependencies += "io.monix" %%% "monix-reactive" % "3.0.0-RC2",
    libraryDependencies += "io.monix" %%% "monix-execution" % "3.0.0-RC2",
    //libraryDependencies += "io.monix" %% "monix-tail" % "3.0.0-RC2",
  )
  .jvmSettings(
    name := "Server",
    //libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.7",
    //libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.19",


  )
  .jsSettings(
    name := "Client",
    // Add JS-specific settings here
  )

lazy val probeJVM = probe.jvm
lazy val probeJS = probe.js
