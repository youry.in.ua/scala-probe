package client

import scala.concurrent.duration._

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel, JSGlobal}
import org.scalajs.dom.document
import org.scalajs.dom.html

import monix.reactive._
import monix.execution.Scheduler.Implicits.global

@JSExportTopLevel("Client")
object Client {


  @JSExport
  def main(id: String): Unit = {
    println(s"Start $id")
    val div = document.getElementById(id).asInstanceOf[html.Div]
    div.textContent = "Hello!"

    val tick =
      Observable.interval(1.second)
        // common filtering and mapping
        .filter(_ % 2 == 0)
        .map(_ * 2)
        // any respectable Scala type has flatMap, w00t!
        .flatMap(x => Observable.fromIterable(Seq(x,x, x + x)))
        // only take the first 5 elements, then stop
        .take(10)
        // to print the generated events to console
        .dump("Out")
        .map(e => div.textContent += s"+${e}")

    val cancelable = tick.subscribe
  }

}
