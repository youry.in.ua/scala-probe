ThisBuild / scalaVersion := ScalaConfig.version //"2.12.7"
ThisBuild / organization := BuildConfig.organization //"gitlab.com/youry.in.ua/scala-probe"

//resolvers += "amateras-repo" at "http://amateras.sourceforge.jp/mvn/"

lazy val root = (project in file("."))
  .dependsOn(scalajs)
  .settings(
    name := BuildConfig.appName, //"Hello",
    LogConfig.logDirKey := LogConfig.logDir,
    // The build info plugin writes these values into a BuildInfo object in the build info package
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion, LogConfig.logDirKey),
    // The logback.groovy config file uses this build info to configure logging, for example
    buildInfoPackage := "appbuildinfo",

    libraryDependencies += "biz.enef" %% "slogging-slf4j" % "0.5.2",
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.8",
    libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.5.19",
    libraryDependencies += "org.codehaus.groovy" % "groovy-all" % "2.4.7",
    libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.7",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.19",
    //libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7",
    libraryDependencies += "io.spray" %%  "spray-json" % "1.3.4",
    libraryDependencies += "com.lihaoyi" %% "scalatags" % "0.6.7",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test,
    //libraryDependencies += "io.monix" %% "monix-reactive" % "3.0.0-RC2",
    //libraryDependencies += "io.monix" %% "monix-execution" % "3.0.0-RC2",

    (resources in Compile) += (fastOptJS in (scalajs, Compile)).value.data,


  ).enablePlugins(BuildInfoPlugin)

  lazy val scalajs = (project in file("scalajs"))
    .settings(
      name := "Client",

      libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.1",
      libraryDependencies += "com.lihaoyi" %%% "scalatags" % "0.6.7",
      libraryDependencies += "io.monix" %%% "monix-reactive" % "3.0.0-RC2",
      libraryDependencies += "io.monix" %%% "monix-execution" % "3.0.0-RC2",


      //libraryDependencies += "com.scalawarrior" %%% "scalajs-createjs" % "0.0.1-SNAPSHOT",
    ).enablePlugins(ScalaJSPlugin)
