// Make information about our build, such as the application name, available in the application.
// We use this, for example, to get the name of the JavaScript file that contains the transpiled Scala of our app.
// https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.7.0")
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.26")
//addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")
