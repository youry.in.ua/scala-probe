package client

import scala.scalajs.js
import scala.scalajs.js.JSON
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel, JSGlobal}

import org.scalajs.dom
import org.scalajs.dom.document
import org.scalajs.dom.html
import org.scalajs.dom.raw.{ Element, HTMLElement, Event }

@js.native
@JSGlobal("createjs.Shape")
class Shape extends js.Object {
  var x: Int = js.native
  var y: Int = js.native

  val graphics: Graphics = js.native

  def addEventListener(`type`: String, listener: js.Function1[MouseEvent, _], useCapture: Boolean = ???): Unit = js.native
  def removeEventListener(`type`: String, listener: js.Function1[MouseEvent, _]): Unit = js.native
}

@js.native
trait Graphics extends js.Object {
  def beginStroke(s: String): Unit = js.native
  def beginFill(s: String): Unit = js.native
  def drawRoundRect(x: Int, y: Int, width: Int, height: Int, ur: Int): Unit = js.native
  def endFill(): Unit = js.native
  def endStroke(): Unit = js.native
  def drawRect(x: Int, y: Int, width: Int, height: Int): Unit = js.native
  def moveTo(x: Int, y: Int): Unit = js.native
  def lineTo(x: Int, y: Int): Unit = js.native
  def drawCircle(x: Int, y: Int, r: Int): Unit = js.native
  def clear(): Unit = js.native

}

@js.native
@JSGlobal("createjs.Stage")
class Stage(id: String) extends js.Object {
  var scale: Double = js.native
  var snapToPixelEnabled: Boolean = js.native

  def addChild(child: Shape*): Unit = js.native
  def update(): Unit = js.native
  def removeAllChildren(): Unit = js.native
}

@js.native
@JSGlobal("createjs.MouseEvent")
class MouseEvent protected () extends Event {
  var stageX: Int = js.native
  var stageY: Int = js.native
}

@js.native
trait CreateJs extends js.Object {
}

@js.native
trait History extends js.Object {
  def go(i: Int): Unit = js.native
}

//import monix.execution.Scheduler.Implicits.global
//import monix.eval._
//import monix.execution._


@JSExportTopLevel("Client")
object Client {

  var ready = false
  var busy = false

  val d = 31
  val n = 15
  val p = 8
  val py = 27
  val ur = 12
  val l = d * n
  val width = d * n + d + (p + 1) * 2
  val height = d * n + d + (p * 2 + 1 + 35) + (p * 2 + 1 + 18)
  val cx = (0 to 14).map {i => i * d + d + p + 1}
  val cy = (0 to 14).map {i => i * d + d + (p + 1) * 2 + py}
  val mode = "Black"
  val cc = "rgba(128,128,128,1)"
  val b = "rgba(0,0,0,1)"
  val w = "rgba(255,255,255,1)"
  val g = "rgba(128,128,128,1)"
  val cb = Array(b, g)
  val cw = Array(g, w)
  val cs = Array(b, w)

  var player = 1
  var steps = Array(Array(7,7))

  val xhr = new dom.XMLHttpRequest()

  var canvas: html.Canvas = null
  var dlg: html.Div  = null//document.getElementById(GameOver)
  var close: html.Span  = null//document.getElementById(Close)
  var info: html.Div = null

  val global = js.Dynamic.global
  val history = global.history.asInstanceOf[History]
  //val JSON = global.JSON
  //val module = global.createjs.asInstanceOf[CreateJs]

  var tools: Shape = new Shape
  var desk: Shape = new Shape
  var stage: Stage = null

  implicit val ctx = monix.execution.Scheduler.Implicits.global
  import monix.execution._
  import monix.reactive._
  import monix.eval._
  import monix.reactive.subjects.Var

  val status = Var("play")

  val cbReq = new Callback[Throwable, dom.XMLHttpRequest] {

    def onSuccess(r: dom.XMLHttpRequest) = {
      //val r = result.asInstanceOf[dom.XMLHttpRequest]
      println(s"Response: ${r.responseText}")
      val resp = JSON.parse(r.responseText)
      //steps = resp.game.asInstanceOf[js.Array[js.Array[Int]]].map(e => Array(e(0).asInstanceOf[Int], e(1).asInstanceOf[Int]))
      val sts1 = resp.game.asInstanceOf[js.Array[js.Array[Int]]] // for (s <- resp.game.asInstanceOf[js.Array[]]) yield(s.toArray[Int]).toArray
      steps = (for (s <- sts1) yield(s.toArray[Int])).toArray //sts1.map(e => e.toArray[Int]).toArray[Array[Array[Int]]]

      redraw

      busy = false

      status := resp.status.asInstanceOf[String]
      //if (status == "over")
      //  dlg.style.display = "block"
    }

    def onError(e: Throwable) = {
      println(e.getMessage)

    }
    //  global.reportFailure(e)
  }

  val clickTools = EventListener(tools, "click")
  .collect { case e: MouseEvent => e }
  .foreach(event => {
      if (!busy) {
        status.exists(_ != "over")
        .foreach(isPlay =>
          if(isPlay) {
            println(s"Tools ${event.`type`}:(${event.stageX },${event.stageY})")
            player = 1 - player
            requestStep
          }
        )
      }
    }
  )

  val clickDesk = EventListener(desk, "click")
  .collect { case e: MouseEvent => e }
  .foreach(event => {
      if (!busy) {
        status.exists(_ != "over")
        .foreach(
          isPlay =>  {
            println(s"Desk ${event.`type`}:(${event.stageX },${event.stageY})")
            val s = getPoint(event.stageX, event.stageY)
            println(s"Point:(${s(0)},${s(1)})")
            if ((0 to 14).contains(s(0)) && (0 to 14).contains(s(1)) && steps.filter(e => e(0) == s(0) && e(1) == s(1)).length == 0) {
              steps = steps ++ Array(s)
              redraw
              requestStep
            }
          }
        )
      }
    }
  )

  @JSExport
  def main(cnv: String, dl: String, cl: String, inf: String): Unit = {
    println("Start")

    canvas = document.getElementById(cnv).asInstanceOf[html.Canvas]
    dlg = document.getElementById(dl).asInstanceOf[html.Div]
    close = document.getElementById(cl).asInstanceOf[html.Span]
    info = document.getElementById(inf).asInstanceOf[html.Div]

    //val ctx = canvas.getContext("2d")
    //                .asInstanceOf[dom.CanvasRenderingContext2D]
    //ctx.scale(1, 1)

    canvas.width = 514
    canvas.height = 583

    status.dump("Status:")
      .doOnNext({ s =>
        Task(if (s == "over") dlg.style.display = "block")
      })
      .subscribe

    close.onclick = (e: dom.MouseEvent) => {
      dlg.style.display = "none"
      history.go(0)
    }

    //ctx.strokeRect(5, 5, 25, 15);
    //ctx.scale(2, 2);
    //ctx.strokeRect(5, 5, 25, 15);

    //println(s"Canvas: ${canvas.getAttribute("id")}")
    //println(s"Canvas id: ${canvas.id}")
    //println(s"module: ${module}")
    init(canvas.id)
    run
  }

  def init(id: String) = {
    println(s"Initialize $id")

    draw(id)
    /*
    tools.addEventListener("click", (event: MouseEvent) => {
        if (!(busy || status == "over")) {
          println(s"Tools ${event.`type`}:(${event.stageX },${event.stageY})")
          player = 1 - player
          requestStep
        }
      }
    )
    */
    /*
    desk.addEventListener("click", (event: MouseEvent) => {
        if (!(busy || status == "over")) {
          println(s"Desk ${event.`type`}:(${event.stageX },${event.stageY})")
          val s = getPoint(event.stageX, event.stageY)
          println(s"Point:(${s(0)},${s(1)})")
          if ((0 to 14).contains(s(0)) && (0 to 14).contains(s(1)) && steps.filter(e => e(0) == s(0) && e(1) == s(1)).length == 0) {
            steps = steps ++ Array(s)
            redraw
            requestStep
          }

        }
      }
    )
    */
/*
    xhr.onreadystatechange = (e) => {
      if (xhr.readyState == 4) {
        println(s"Response: ${xhr.responseText}")
        val resp = JSON.parse(xhr.responseText)
        //steps = resp.game.asInstanceOf[js.Array[js.Array[Int]]].map(e => Array(e(0).asInstanceOf[Int], e(1).asInstanceOf[Int]))
        val sts1 = resp.game.asInstanceOf[js.Array[js.Array[Int]]] // for (s <- resp.game.asInstanceOf[js.Array[]]) yield(s.toArray[Int]).toArray
        steps = (for (s <- sts1) yield(s.toArray[Int])).toArray //sts1.map(e => e.toArray[Int]).toArray[Array[Array[Int]]]
        status = resp.status.asInstanceOf[String]

        redraw

        busy = false
        if (status == "over")
          dlg.style.display = "block"
      }
    }
*/
    requestStep

    ready = true
    println("Initialized")
  }

  def draw(id: String) = {
    drawInit(id)
    drawGrid
    drawTools
    drawSteps

    println(s"Stage scale: ${stage.scale}")
    stage.addChild(desk, tools)
    stage.update
    println(s"Stage after update scale: ${stage.scale}, snapToPixelEnabled: ${stage.snapToPixelEnabled}")
  }

  def redraw() = {
    drawClear
    drawGrid
    drawTools
    drawSteps

    stage.addChild(desk, tools)
    stage.update
  }

  def drawInit(id: String) = {
    //tools = js.Dynamic.newInstance(module.Shape)()
    //tools = new Shape
    //desk = js.Dynamic.newInstance(module.Shape)()
    //desk = new Shape
    //stage = js.Dynamic.newInstance(module.Stage)(id)
    stage = new Stage(id)
    stage.snapToPixelEnabled = true
  }

  def drawClear() = {
    tools.graphics.clear
    desk.graphics.clear
    stage.removeAllChildren
  }

  def drawGrid() = {

    desk.x = 0
    desk.y = 0

    val g = desk.graphics
    g.beginStroke("rgba(16,16,16,1)")
    g.beginFill("rgba(64,64,64,1)")
    g.drawRoundRect(0, 0, width, height, ur)
    g.endFill
    g.endStroke
    g.beginStroke("rgba(64,64,64,1)")
    g.beginFill("rgba(224,224,224,1)")
    g.drawRect(p + 1, (p + 1) * 2 + py, l + d, l + d)
    g.endFill
    g.endStroke
    g.beginStroke("rgba(4,4,4,0.2)")

    (0 to 14).map( i => {
        g.moveTo(d + p + 1, cy(i))
        g.lineTo(l + p + 1, cy(i))
        g.moveTo(cx(i), d + (p + 1) * 2 + py)
        g.lineTo(cx(i), l + (p + 1) * 2 + py)
      }
    )

    g.endStroke
  }

  def drawTools() = {
    tools.x = 0
    tools.y = 0

    val g = tools.graphics
    g.beginStroke(cc)
    g.beginFill(cb(player))
    g.drawCircle(p * 2 + py / 2, p + py / 2, py / 2)
    g.endFill
    g.endStroke
    g.beginStroke(cc)
    g.beginFill(cw(player))
    g.drawCircle(p * 4 + py + py / 2, p + py / 2, py / 2)
    g.endFill
    g.endStroke
  }

  def drawSteps() = {
    for (i <- 0 to steps.length - 1) {
      val color = i % 2
      val x = steps(i)(0)
      val y = steps(i)(1)
      //console.log "#{i}:#{s}-#{color}"
      val g = desk.graphics
      g.beginStroke(cc)
      g.beginFill(cs(color))
      g.drawCircle(cx(x), cy(y), d / 2)
      g.endFill
      g.endStroke
    }
  }

  def requestStep() = {

    import scala.concurrent.duration._
    import js.JSConverters._

    implicit val ctx = monix.execution.Scheduler.Implicits.global

    if (!busy) {

    val curr = steps.length % 2

    if (curr != player) {
        busy = true
        println(s"Request step: ${steps.length}")
        //xhr.open("POST", "api", true)
        val req = JSON.stringify(js.Dynamic.literal(game = steps.map(_.toJSArray).toJSArray))
        //xhr.send(req)
        val task = Ajax("POST", "api", req, "", Map[String, String](), false, "")(Ajax.Timeout(3.seconds))
        //val future = task.runToFuture
        val cancelable = task.runAsync(cbReq)
      }
    }
  }

  def getPoint(px: Int, py: Int): Array[Int] = {
    val x = cx.indexWhere((e) => px < e + d / 2 && px > e - d / 2)
    val y = cy.indexWhere((e) => py < e + d / 2 && py > e - d / 2)

    println(s"GetPoint: ${x}, ${y}")
    Array(x, y)
  }

  def title =
    s"Application: ${canvas.id}"

  def run =
    println(title)
}
