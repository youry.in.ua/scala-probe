package client

import scala.concurrent.duration._

import scala.scalajs.js

import org.scalajs.dom
import org.scalajs.dom.{Event, FormData, XMLHttpRequest}

import monix.eval._
import monix.execution._

object Ajax {

  case class Timeout(value: Duration)

  def apply(method: String,
            url: String,
            data: String,
            queryParams: String,
            headers: Map[String, String],
            withCredentials: Boolean,
            responseType: String)(implicit timeout: Timeout): Task[dom.XMLHttpRequest] = {

    println(s"Ajax request: ${method}, ${url}, ${data}")

    Task.create[dom.XMLHttpRequest] { (_, callback) =>
      //println(s"Task create: ${callback}")

      val req = new dom.XMLHttpRequest()

      req.onreadystatechange = { (_: Event) =>
        if (req.readyState == 4) {
          if ((req.status >= 200 && req.status < 300) || req.status == 304)
            //println(s"onnreadystatechange responseText: ${req.responseText}")
            callback.onSuccess(req)
          else
            callback.onError(new Throwable(s"XMLHttpRequest error: ${req.status}")) //(QQRuntimeException(NonEmptyList(AjaxException(req, url), Nil)))
        }
      }

      val urlWithQuery = url //addQueryParams(url, queryParams)
      // TODO: Remove this shit when scala.js issue #2631 is fixed, crashes scoverage
      req.asInstanceOf[js.Dynamic].open(method, urlWithQuery)

      req.responseType = responseType
      req.asInstanceOf[js.Dynamic].timeout = if (timeout.value.isFinite()) timeout.value.toMillis.toInt else 0
      req.withCredentials = withCredentials
      headers.foreach((req.setRequestHeader _).tupled)
      if (data == null)
        req.asInstanceOf[js.Dynamic].send()
      else
        req.send(data)

      Cancelable(() => req.abort())
    }

  }


}
