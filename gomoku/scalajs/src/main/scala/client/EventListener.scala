package client

import org.scalajs.dom.{Event, EventTarget}

import monix.reactive.Observable
import monix.reactive.OverflowStrategy.Unbounded
import monix.execution.{Cancelable, Ack}
import monix.execution.cancelables.SingleAssignmentCancelable

object EventListener {
  implicit val ctx = monix.execution.Scheduler.Implicits.global

  def apply(target: Shape, event: String): Observable[Event] =
    Observable.create(Unbounded) { subscriber =>
      val c = SingleAssignmentCancelable()
      // Forced conversion, otherwise canceling will not work!
      val f: scalajs.js.Function1[Event,Ack] = (e: Event) =>
        subscriber.onNext(e).syncOnStopOrFailure(_ => c.cancel())

        target.addEventListener(event, f)
        c := Cancelable(() => target.removeEventListener(event, f))
      }

}
