class App
  (@module, @name, @url, @header) ->
    @ready = false
    @busy = false
    @status = \play
    @d = 31
    @n = 15
    @p = 8
    @py = 27
    @ur = 12
    @l = @d * @n
    @width = @d * @n + @d + (@p + 1) * 2
    @height = @d * @n + @d + (@p * 2 + 1 + 35) + (@p * 2 + 1 + 18)
    @cx = [0 til 15].map (i) ~> i * @d + @d + @p + 1
    @cy = [0 til 15].map (i) ~> i * @d + @d + (@p + 1) * 2 + @py
    @mode = "Black"
    @cc = "rgba(128,128,128,1)"
    @b = "rgba(0,0,0,1)"
    @w = "rgba(255,255,255,1)"
    @g = "rgba(128,128,128,1)"
    @cb = [@b, @g]
    @cw = [@g, @w]
    @cs = [@b, @w]
    @player = 1
    @steps = [[7,7]]
    @xhr = new XMLHttpRequest!
    @dlg = document.getElementById \GameOver
    @close = document.getElementById \Close
    @close.onclick = ~>
      @dlg.style.display = \none
      history.go 0

  title: ->
    "Application: #{@name}"

  run: ->
    console.log this.title()

  newButton: (id, text, onpressed) ->
    button = new zebra.ui.Button(text)
    button.id = id
    button.bind onpressed
    button

  newCombo: (id, list, selected, onselect) ->
    combo = new zebra.ui.Combo(list)
    combo.id = id
    combo.setValue selected
    combo.bind onselect
    combo

  draw: (id) ->
    @drawInit id
    @drawGrid!
    @drawTools!
    @drawSteps!

    @stage
      ..addChild @desk, @tools
      ..update!

  redraw: ->
    @drawClear
    @drawGrid!
    @drawTools!
    @drawSteps!

    @stage
      ..addChild @desk, @tools
      ..update!

  drawInit: (id) ->
    @tools = new @module.Shape!
    @desk = new @module.Shape!
    @stage = new @module.Stage id

  drawClear: ->
    @tools
      ..graphics
        ..clear!
    @desk
      ..graphics
        ..clear!
    @stage
      ..removeAllChildren!


  drawGrid: ->
    @desk
      ..x = 0
      ..y = 0
      ..graphics
        ..beginStroke "rgba(16,16,16,1)"
        ..beginFill "rgba(64,64,64,1)"
        ..drawRoundRect 0, 0, @width, @height, @ur
        ..endFill!
        ..endStroke!
        ..beginStroke "rgba(64,64,64,1)"
        ..beginFill "rgba(224,224,224,1)"
        ..drawRect @p + 1, (@p + 1) * 2 + @py, @l + @d, @l + @d
        ..endFill!
        ..endStroke!
        ..beginStroke "rgba(4,4,4,0.2)"

    [0 til 15].map (i) ~>
        @desk.graphics
            ..moveTo @d + @p + 1, @cy[i]
            ..lineTo @l + @p + 1, @cy[i]
            ..moveTo @cx[i], @d + (@p + 1) * 2 + @py
            ..lineTo @cx[i], @l + (@p + 1) * 2 + @py

    @desk.graphics.endStroke!

  drawTools: ->
    @tools
      ..x = 0
      ..y = 0
      ..graphics
        ..beginStroke @cc
        ..beginFill @cb[@player]
        ..drawCircle @p * 2 + @py / 2, @p + @py / 2, @py / 2
        ..endFill!
        ..endStroke!
        ..beginStroke @cc
        ..beginFill @cw[@player]
        ..drawCircle @p * 4 + @py + @py / 2, @p + @py / 2, @py / 2
        ..endFill!
        ..endStroke!

  drawSteps: ->
    for s, i in @steps
      color = i % 2
      #console.log "#{i}:#{s}-#{color}"
      @desk
        ..graphics
          ..beginStroke @cc
          ..beginFill @cs[color]
          ..drawCircle @cx[s[0]], @cy[s[1]], @d / 2
          ..endFill!
          ..endStroke!

  requestStep: ->
    if @busy
      return
    curr = @steps.length % 2
    if curr != @player
      @busy = true
      console.log "Request step: #{@steps.length}"
      @xhr.open \POST, \api, true
      JSON.stringify \game : @steps
      |> @xhr.send

  getPoint: (cx, cy) ->
    x = @cx.findIndex (e) ~>
      cx < e + @d / 2 and cx > e - @d / 2
    y = @cy.findIndex (e) ~>
      cy < e + @d / 2 and cy > e - @d / 2
    console.log "GetPoint: #{x}, #{y}"
    [x,y]

  status: (text) ->
    text

  init: (id) ->


    @draw id

    @tools.addEventListener \click, (event) ~>
      if @busy or @status == \over
        return
      console.log "Tools #{event.type}:(#{event.stageX },#{event.stageY})"
      @player = 1 - @player
      @requestStep!

    @desk.addEventListener \click, (event) ~>
      if @busy or @status == \over
        return
      console.log "Desk #{event.type}:(#{event.stageX },#{event.stageY})"
      s = @getPoint event.stageX, event.stageY
      if s[0] in [0 til 15] and s[1] in [0 til 15] and @steps.findIndex((e) -> e[0] == s[0] and e[1] == s[1]) == -1
        @steps[*] = s
        @requestStep!

    @xhr.onreadystatechange = ~>
      if @xhr.readyState == 4
        console.log @xhr.responseText
        resp = JSON.parse @xhr.responseText
        @steps = resp.game
        @status = resp.status
        @redraw!
        @busy = false
        if @status == \over
          @dlg.style.display = \block

    @requestStep!

    @ready = true
    console.log('Initialized')

module.exports = App
