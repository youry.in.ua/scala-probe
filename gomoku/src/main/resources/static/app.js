(function() {
  'use strict';

  var globals = typeof window === 'undefined' ? global : window;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};
  var aliases = {};
  var has = ({}).hasOwnProperty;

  var expRe = /^\.\.?(\/|$)/;
  var expand = function(root, name) {
    var results = [], part;
    var parts = (expRe.test(name) ? root + '/' + name : name).split('/');
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function expanded(name) {
      var absolute = expand(dirname(path), name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var hot = null;
    hot = hmr && hmr.createHot(name);
    var module = {id: name, exports: {}, hot: hot};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var expandAlias = function(name) {
    return aliases[name] ? expandAlias(aliases[name]) : name;
  };

  var _resolve = function(name, dep) {
    return expandAlias(expand(dirname(name), dep));
  };

  var require = function(name, loaderPath) {
    if (loaderPath == null) loaderPath = '/';
    var path = expandAlias(name);

    if (has.call(cache, path)) return cache[path].exports;
    if (has.call(modules, path)) return initModule(path, modules[path]);

    throw new Error("Cannot find module '" + name + "' from '" + loaderPath + "'");
  };

  require.alias = function(from, to) {
    aliases[to] = from;
  };

  var extRe = /\.[^.\/]+$/;
  var indexRe = /\/index(\.[^\/]+)?$/;
  var addExtensions = function(bundle) {
    if (extRe.test(bundle)) {
      var alias = bundle.replace(extRe, '');
      if (!has.call(aliases, alias) || aliases[alias].replace(extRe, '') === alias + '/index') {
        aliases[alias] = bundle;
      }
    }

    if (indexRe.test(bundle)) {
      var iAlias = bundle.replace(indexRe, '');
      if (!has.call(aliases, iAlias)) {
        aliases[iAlias] = bundle;
      }
    }
  };

  require.register = require.define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has.call(bundle, key)) {
          require.register(key, bundle[key]);
        }
      }
    } else {
      modules[bundle] = fn;
      delete cache[bundle];
      addExtensions(bundle);
    }
  };

  require.list = function() {
    var list = [];
    for (var item in modules) {
      if (has.call(modules, item)) {
        list.push(item);
      }
    }
    return list;
  };

  var hmr = globals._hmr && new globals._hmr(_resolve, require, modules, cache);
  require._cache = cache;
  require.hmr = hmr && hmr.wrap;
  require.brunch = true;
  globals.require = require;
})();

(function() {
var global = window;
var __makeRelativeRequire = function(require, mappings, pref) {
  var none = {};
  var tryReq = function(name, pref) {
    var val;
    try {
      val = require(pref + '/node_modules/' + name);
      return val;
    } catch (e) {
      if (e.toString().indexOf('Cannot find module') === -1) {
        throw e;
      }

      if (pref.indexOf('node_modules') !== -1) {
        var s = pref.split('/');
        var i = s.lastIndexOf('node_modules');
        var newPref = s.slice(0, i).join('/');
        return tryReq(name, newPref);
      }
    }
    return none;
  };
  return function(name) {
    if (name in mappings) name = mappings[name];
    if (!name) return;
    if (name[0] !== '.' && pref) {
      var val = tryReq(name, pref);
      if (val !== none) return val;
    }
    return require(name);
  }
};
require.register("game.ls", function(exports, require, module) {
var App;
App = (function(){
  App.displayName = 'App';
  var prototype = App.prototype, constructor = App;
  function App(module, name, url, header){
    var this$ = this;
    this.module = module;
    this.name = name;
    this.url = url;
    this.header = header;
    this.ready = false;
    this.busy = false;
    this.status = 'play';
    this.d = 31;
    this.n = 15;
    this.p = 8;
    this.py = 27;
    this.ur = 12;
    this.l = this.d * this.n;
    this.width = this.d * this.n + this.d + (this.p + 1) * 2;
    this.height = this.d * this.n + this.d + (this.p * 2 + 1 + 35) + (this.p * 2 + 1 + 18);
    this.cx = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14].map(function(i){
      return i * this$.d + this$.d + this$.p + 1;
    });
    this.cy = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14].map(function(i){
      return i * this$.d + this$.d + (this$.p + 1) * 2 + this$.py;
    });
    this.mode = "Black";
    this.cc = "rgba(128,128,128,1)";
    this.b = "rgba(0,0,0,1)";
    this.w = "rgba(255,255,255,1)";
    this.g = "rgba(128,128,128,1)";
    this.cb = [this.b, this.g];
    this.cw = [this.g, this.w];
    this.cs = [this.b, this.w];
    this.player = 1;
    this.steps = [[7, 7]];
    this.xhr = new XMLHttpRequest();
    this.dlg = document.getElementById('GameOver');
    this.close = document.getElementById('Close');
    this.close.onclick = function(){
      this$.dlg.style.display = 'none';
      return history.go(0);
    };
  }
  App.prototype.title = function(){
    return "Application: " + this.name;
  };
  App.prototype.run = function(){
    return console.log(this.title());
  };
  App.prototype.newButton = function(id, text, onpressed){
    var button;
    button = new zebra.ui.Button(text);
    button.id = id;
    button.bind(onpressed);
    return button;
  };
  App.prototype.newCombo = function(id, list, selected, onselect){
    var combo;
    combo = new zebra.ui.Combo(list);
    combo.id = id;
    combo.setValue(selected);
    combo.bind(onselect);
    return combo;
  };
  App.prototype.draw = function(id){
    var x$;
    this.drawInit(id);
    this.drawGrid();
    this.drawTools();
    this.drawSteps();
    x$ = this.stage;
    x$.addChild(this.desk, this.tools);
    x$.update();
    return x$;
  };
  App.prototype.redraw = function(){
    var x$;
    this.drawClear;
    this.drawGrid();
    this.drawTools();
    this.drawSteps();
    x$ = this.stage;
    x$.addChild(this.desk, this.tools);
    x$.update();
    return x$;
  };
  App.prototype.drawInit = function(id){
    this.tools = new this.module.Shape();
    this.desk = new this.module.Shape();
    return this.stage = new this.module.Stage(id);
  };
  App.prototype.drawClear = function(){
    var x$, y$, z$, z1$, z2$;
    x$ = this.tools;
    y$ = x$.graphics;
    y$.clear();
    z$ = this.desk;
    z1$ = z$.graphics;
    z1$.clear();
    z2$ = this.stage;
    z2$.removeAllChildren();
    return z2$;
  };
  App.prototype.drawGrid = function(){
    var x$, y$, this$ = this;
    x$ = this.desk;
    x$.x = 0;
    x$.y = 0;
    y$ = x$.graphics;
    y$.beginStroke("rgba(16,16,16,1)");
    y$.beginFill("rgba(64,64,64,1)");
    y$.drawRoundRect(0, 0, this.width, this.height, this.ur);
    y$.endFill();
    y$.endStroke();
    y$.beginStroke("rgba(64,64,64,1)");
    y$.beginFill("rgba(224,224,224,1)");
    y$.drawRect(this.p + 1, (this.p + 1) * 2 + this.py, this.l + this.d, this.l + this.d);
    y$.endFill();
    y$.endStroke();
    y$.beginStroke("rgba(4,4,4,0.2)");
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14].map(function(i){
      var x$;
      x$ = this$.desk.graphics;
      x$.moveTo(this$.d + this$.p + 1, this$.cy[i]);
      x$.lineTo(this$.l + this$.p + 1, this$.cy[i]);
      x$.moveTo(this$.cx[i], this$.d + (this$.p + 1) * 2 + this$.py);
      x$.lineTo(this$.cx[i], this$.l + (this$.p + 1) * 2 + this$.py);
      return x$;
    });
    return this.desk.graphics.endStroke();
  };
  App.prototype.drawTools = function(){
    var x$, y$;
    x$ = this.tools;
    x$.x = 0;
    x$.y = 0;
    y$ = x$.graphics;
    y$.beginStroke(this.cc);
    y$.beginFill(this.cb[this.player]);
    y$.drawCircle(this.p * 2 + this.py / 2, this.p + this.py / 2, this.py / 2);
    y$.endFill();
    y$.endStroke();
    y$.beginStroke(this.cc);
    y$.beginFill(this.cw[this.player]);
    y$.drawCircle(this.p * 4 + this.py + this.py / 2, this.p + this.py / 2, this.py / 2);
    y$.endFill();
    y$.endStroke();
    return x$;
  };
  App.prototype.drawSteps = function(){
    var i$, ref$, len$, i, s, color, x$, y$, results$ = [];
    for (i$ = 0, len$ = (ref$ = this.steps).length; i$ < len$; ++i$) {
      i = i$;
      s = ref$[i$];
      color = i % 2;
      x$ = this.desk;
      y$ = x$.graphics;
      y$.beginStroke(this.cc);
      y$.beginFill(this.cs[color]);
      y$.drawCircle(this.cx[s[0]], this.cy[s[1]], this.d / 2);
      y$.endFill();
      y$.endStroke();
      results$.push(x$);
    }
    return results$;
  };
  App.prototype.requestStep = function(){
    var curr;
    if (this.busy) {
      return;
    }
    curr = this.steps.length % 2;
    if (curr !== this.player) {
      this.busy = true;
      console.log("Request step: " + this.steps.length);
      this.xhr.open('POST', 'api', true);
      return this.xhr.send(
      JSON.stringify({
        'game': this.steps
      }));
    }
  };
  App.prototype.getPoint = function(cx, cy){
    var x, y, this$ = this;
    x = this.cx.findIndex(function(e){
      return cx < e + this$.d / 2 && cx > e - this$.d / 2;
    });
    y = this.cy.findIndex(function(e){
      return cy < e + this$.d / 2 && cy > e - this$.d / 2;
    });
    console.log("GetPoint: " + x + ", " + y);
    return [x, y];
  };
  App.prototype.status = function(text){
    return text;
  };
  App.prototype.init = function(id){
    var this$ = this;
    this.draw(id);
    this.tools.addEventListener('click', function(event){
      if (this$.busy || this$.status === 'over') {
        return;
      }
      console.log("Tools " + event.type + ":(" + event.stageX + "," + event.stageY + ")");
      this$.player = 1 - this$.player;
      return this$.requestStep();
    });
    this.desk.addEventListener('click', function(event){
      var s, ref$;
      if (this$.busy || this$.status === 'over') {
        return;
      }
      console.log("Desk " + event.type + ":(" + event.stageX + "," + event.stageY + ")");
      s = this$.getPoint(event.stageX, event.stageY);
      if (((ref$ = s[0]) === 0 || ref$ === 1 || ref$ === 2 || ref$ === 3 || ref$ === 4 || ref$ === 5 || ref$ === 6 || ref$ === 7 || ref$ === 8 || ref$ === 9 || ref$ === 10 || ref$ === 11 || ref$ === 12 || ref$ === 13 || ref$ === 14) && ((ref$ = s[1]) === 0 || ref$ === 1 || ref$ === 2 || ref$ === 3 || ref$ === 4 || ref$ === 5 || ref$ === 6 || ref$ === 7 || ref$ === 8 || ref$ === 9 || ref$ === 10 || ref$ === 11 || ref$ === 12 || ref$ === 13 || ref$ === 14) && this$.steps.findIndex(function(e){
        return e[0] === s[0] && e[1] === s[1];
      }) === -1) {
        (ref$ = this$.steps)[ref$.length] = s;
        return this$.requestStep();
      }
    });
    this.xhr.onreadystatechange = function(){
      var resp;
      if (this$.xhr.readyState === 4) {
        console.log(this$.xhr.responseText);
        resp = JSON.parse(this$.xhr.responseText);
        this$.steps = resp.game;
        this$.status = resp.status;
        this$.redraw();
        this$.busy = false;
        if (this$.status === 'over') {
          return this$.dlg.style.display = 'block';
        }
      }
    };
    this.requestStep();
    this.ready = true;
    return console.log('Initialized');
  };
  return App;
}());
module.exports = App;
});

;require.register("initialize.js", function(exports, require, module) {
document.addEventListener('DOMContentLoaded', function() {
  console.log("Initialize: DOMContentLoaded");
/*
  var stage = new createjs.Stage("canvas-gomoku");

  var desk = new createjs.Shape();
  desk.graphics.beginFill("lightgrey").drawRect(0, 0, 50, 50);
  desk.x = 100;
  desk.y = 100;

  stage.addChild(desk);
  stage.update();
*/

	var App = require	("game");
  var app = new App(createjs, "Gomoku!", "http://localhost:4000/api", {"Content-Type": "application/json"});
  app.init("canvas-gomoku");
  app.run();

});

});

require.register("___globals___", function(exports, require, module) {
  
});})();require('___globals___');


//# sourceMappingURL=app.js.map