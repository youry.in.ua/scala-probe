package gomoku

import appbuildinfo.BuildInfo
import slogging._

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import akka.http.scaladsl.model._
import akka.http.scaladsl.Http
//import akka.http.scaladsl.model.HttpMethods._

import akka.http.scaladsl.server.Directives._

//import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._
import DefaultJsonProtocol._

import scala.io.StdIn

object Hello extends LazyLogging with Format{

  private def printRuntimeInfo() = {
    val runtime = Runtime.getRuntime
    val mb = 1024 * 1024
    val maxMemoryInMb = runtime.maxMemory() / mb
    logger.info("JVM max heap memory: {} MB. Available processors: {}", maxMemoryInMb, runtime.availableProcessors)
  }

  def main(args: Array[String]): Unit = {

    LoggerConfig.factory = SLF4JLoggerFactory()

    logger.info("Let's start the {} server :-)", BuildInfo.name)
    printRuntimeInfo()

    implicit val system = ActorSystem("my-system")
    implicit val materializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.dispatcher

    val route =
      get {
        path(Segment) { name =>
          logger.info("get resource: {}", name)
          getFromResource(s"$name") // uses implicit ContentTypeResolver
        } ~
        path("") {
          logger.info("get : Page.skeleton.render")
          complete{
            HttpEntity(
              ContentTypes.`text/html(UTF-8)`,
              "<!DOCTYPE html>\n" + Page.skeleton.render
            )
          }
        } ~
        path("static" / Segment ) { name =>
          logger.info("get static: {}", name)
          getFromResource(s"static/$name") // uses implicit ContentTypeResolver
        } //~
        //path("") {
        //  logger.info("get static index.html")
        //  getFromResource("static/index.html")
        //}
      } ~
      post {
        path("api") {
          entity(as[String]) { src =>
            val json = src.parseJson
            logger.info(s"post pack: $json")

            val pack = json.convertTo[Pack]

            val resp = calcStep(pack)
            logger.info(s"resp pack: ${resp.toJson}")
            complete(resp.toJson.prettyPrint)
            //complete("{\"status\": \"play\",\"game\":[[7,7], [8,8]]}")
          }
        }
      }

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    logger.info(s"Server online at http://localhost:8080/")

    println("\nPress RETURN to stop...")

    StdIn.readLine()

    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

  }

  def calcStep(pack: Pack): Pack = {

    try {
      Game.calculate(pack)
    } catch {
      case e: Throwable => {
        logger.error(s"calcStep Exception: ${e.toString}")
        Pack(e.toString, pack.game)
      }
    }

  }

}
