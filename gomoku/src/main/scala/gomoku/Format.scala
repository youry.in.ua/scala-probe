package gomoku

import spray.json._
import DefaultJsonProtocol._

trait Format {

  implicit object PackFormat extends RootJsonFormat[Pack] {

    def toGame(v: Array[JsValue]): Array[Tuple2[Int, Int]] = {
      v.map(toStep(_))
    }

    def toStep(v: JsValue) : Tuple2[Int, Int] = {
      v match {
        case JsArray(v) => (v(0).convertTo[Int], v(1).convertTo[Int])
        case _ => throw new DeserializationException("Step expected")
      }
    }

    override def read(value: JsValue) = {
      value.asJsObject.getFields("status", "game") match {
        case Seq(JsString(status), JsArray(game)) =>
          new Pack(status, toGame(game.to[Array]))
        case Seq(JsArray(game)) =>
          new Pack("play", toGame(game.to[Array]))
        case _ => throw new DeserializationException("Pack expected")
      }
    }

    override def write(obj: Pack): JsValue = JsObject(
      "status" -> JsString(obj.status),
      "game" -> JsArray(obj.game.map(_.toJson).toVector),
    )

  }

  implicit object SlotFormat extends RootJsonFormat[Slot] {

    override def read(value: JsValue) = {
      value.asJsObject.getFields("scpx", "scpy", "d", "rs", "ss") match {
        case Seq(JsNumber(scpx), JsNumber(scpy), JsNumber(d), JsNumber(rs), JsNumber(ss)) =>
          new Slot(scpx.toInt, scpy.toInt, d.toInt, rs.toInt, ss.toInt)
        case _ => throw new DeserializationException("Slot expected")
      }
    }

    override def write(obj: Slot): JsValue = JsObject(
      "scpx" -> JsNumber(obj.scpX),
      "scpy" -> JsNumber(obj.scpY),
      "d" -> JsNumber(obj.d),
      "rs" -> JsNumber(obj.rs),
      "ss" -> JsNumber(obj.ss),
    )

  }

  implicit object PointFormat extends RootJsonFormat[Point] {

    override def read(value: JsValue) = {
      value.asJsObject.getFields("x", "y", "sp") match {
        case Seq(JsNumber(x), JsNumber(y), JsNumber(sp)) =>
          new Point(x.toInt, y.toInt, sp.toInt)
        case _ => throw new DeserializationException("Point expected")
      }
    }

    override def write(obj: Point): JsValue = JsObject(
      "x" -> JsNumber(obj.x),
      "y" -> JsNumber(obj.y),
      "sp" -> JsNumber(obj.sp),
    )
  }

  implicit object UnionFormat extends RootJsonFormat[Union] {

    override def read(value: JsValue) = {
      value.asJsObject.getFields("point", "slot") match {
        case Seq(JsObject(point), JsObject(slot)) =>
          new Union(value.asJsObject("point").convertTo[Point], value.asJsObject("slot").convertTo[Slot])
        case _ => throw new DeserializationException("Point expected")
      }
    }

    override def write(obj: Union): JsValue = JsObject(
      "point" -> obj.point.toJson,
      "slot" -> obj.slot.toJson,
    )
  }

  implicit object StepFormat extends RootJsonFormat[Step] {

    override def read(value: JsValue) = {
      value.asJsObject.getFields("x", "y") match {
        case Seq(JsNumber(x), JsNumber(y)) =>
          new Step(x.toInt, y.toInt)
        case _ => throw new DeserializationException("Step expected")
      }
    }

    override def write(obj: Step): JsValue = JsObject(
      "x" -> JsNumber(obj.x),
      "y" -> JsNumber(obj.y),
    )
  }

  implicit object NetFormat extends RootJsonFormat[Net] {

    override def read(value: JsValue) = {
      value.asJsObject.getFields("slots", "points", "union", "steps") match {
        case Seq(JsArray(slots), JsArray(points), JsArray(union), JsArray(steps)) =>
          new Net(slots.to[Array].map(_.convertTo[Slot]), points.to[Array].map(_.convertTo[Point]), union.to[Array].map(_.convertTo[Union]), steps.to[Array].map(_.convertTo[Step]))
        case _ => throw new DeserializationException("Net expected")
      }
    }

    override def write(obj: Net): JsValue = JsObject(
      "slots" -> JsArray(obj.allSlots.map(_.toJson).toVector),
      "points" -> JsArray(obj.allPoints.map(_.toJson).toVector),
      "union" -> JsArray(obj.union.map(_.toJson).toVector),
      "steps" -> JsArray(obj.steps.map(_.toJson).toVector),
    )

  }



}
