package gomoku

import scalatags.Text.all._

object Page{
  //val boot =
  //  "console.log('onLoad');"
  val skeleton =
    html(
      head(
        meta(charset:="utf-8"),
        meta(name:="viewport", content:="initial-scale=1.0, user-scalable=no"),
        meta(name:="viewport", content:="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi"),
        link(rel:="stylesheet", href:="static/app.css"),
        script(src:="https://code.createjs.com/1.0.0/createjs.min.js"),
        script(src:="client-fastopt.js"),
      ),
      body(
        //onload:=boot,
        div(
          id:="GameOver",
          cls:="modal",
          div(
            cls:="modal-content",
            span(id:="Close", cls:="close", "x"),
            p(style:="align:center", "Game Over!")
          )
         ),
        div(
          style:="display: flex;flex-direction: row;flex-wrap: wrap;justify-content: center;align-items: center;",
          canvas(id:="canvas-gomoku", width:="514px", height:="583px"),
          div(id:="info"),
        ),
        script("document.addEventListener('DOMContentLoaded', function() {Client.main('canvas-gomoku', 'GameOver', 'Close', 'info')})"),
      )
    )
}
