package client

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel, JSGlobal, ScalaJSDefined}
import org.scalajs.dom.document
import org.scalajs.dom.html

import wrappers.Window
import elements._



@JSExportTopLevel("Client")
object Client {

  val window = js.Dynamic.global.window.asInstanceOf[Window]

  @JSExport
  def main(id: String): Unit = {

    window.customElements.define("add-section", js.constructorOf[AddSectionElement])
    window.customElements.define("list-section", js.constructorOf[ListSectionElement])
    window.customElements.define("delete-section", js.constructorOf[DeleteSectionElement])
    window.customElements.define("side-nav", js.constructorOf[SideNavElement])
    window.customElements.define("main-area", js.constructorOf[MainAreaElement])
    window.customElements.define("header-bar", js.constructorOf[HeaderBarElement])
    window.customElements.define("app-element", js.constructorOf[AppElement])
    println("elements registered")

    println(s"Start $id")
    val div = document.getElementById(id).asInstanceOf[html.Div]
    div.textContent = "Hello!"
  }

}
