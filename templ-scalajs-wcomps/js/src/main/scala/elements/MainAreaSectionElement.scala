package elements

import org.scalajs.dom
import org.scalajs.dom.Element
import wrappers.{HTMLElement, HTMLTemplateElement}

import scala.scalajs.js
import scala.scalajs.js.Dynamic.literal
//import scala.scalajs.js.annotation.{ JSExportTopLevel, ScalaJSDefined }

object MainAreaSectionElement {
  val template: HTMLTemplateElement =
    dom.document.getElementById("main-area-section-template").asInstanceOf[HTMLTemplateElement]
}

//@ScalaJSDefined
abstract class MainAreaSectionElement extends HTMLElement {

  val shadow = attachShadow(literal(mode = "open"))

  shadow.appendChild(
    MainAreaSectionElement.template.content.cloneNode(true)
  )

  val content: Element = shadow.querySelector(".container")


  def setName(name: String): Unit = {
    setAttribute("name", name)
  }

  def getName(): String = {
    getAttribute("name")
  }

  def clear(): Unit = {
    var firstChild = content.firstChild
    while (firstChild != null) {
      content.removeChild(firstChild)
      firstChild = content.firstChild
    }
  }

}
