package wrappers

import org.scalajs.dom
import org.scalajs.dom.html.Element

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal


@js.native
trait CustomElementsRegistry extends js.Any {

   def define(name: String, definition: Any ) :Unit = js.native

}

@js.native
@JSGlobal
class Window extends dom.Window {

   val customElements: CustomElementsRegistry = js.native

}

@js.native
@JSGlobal
class Event(eventType: String) extends org.scalajs.dom.raw.Event {}

@js.native
@JSGlobal
class HTMLElement extends Element {
   def attachShadow(options: js.Any) : Element = js.native
}

@js.native
@JSGlobal
class HTMLTemplateElement extends HTMLElement {

  val content: HTMLElement = js.native
}
