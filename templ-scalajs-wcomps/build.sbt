
import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}
import org.scalajs.core.tools.linker.standard._

name := "Templ root project"

scalaVersion in ThisBuild := "2.12.7"

lazy val root = project.in(file("."))
  .aggregate(templJS, templJVM)
  .dependsOn(templJS, templJVM)
  .settings(
    publish := {},
    publishLocal := {},
    libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.7",
    libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.19",
    resources in Compile += (fastOptJS in Compile in templJS).value.data,
    //mainClass in Compile := (mainClass in Compile in templJVM).value,
  )

lazy val templ = crossProject(JSPlatform, JVMPlatform).in(file("."))

  .settings(
    name := "Templ",
    version := "0.1-SNAPSHOT",
    libraryDependencies += "com.lihaoyi" %%% "scalatags" % "0.6.7",
    libraryDependencies ++= Seq(
      "io.circe" %%% "circe-core",
      "io.circe" %%% "circe-generic",
      "io.circe" %%% "circe-parser"
    ).map(_ % "0.9.3"),
  )
  .jvmSettings(
    name := "Server",
    //libraryDependencies += "com.typesafe.akka" %% "akka-http"   % "10.1.7",
    //libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.19",


  )
  .jsSettings(
    name := "Client",
    // Add JS-specific settings here
    //scalaJSLinkerConfig ~= { _.withOutputMode(OutputMode.ECMAScript2015) },
    scalaJSLinkerConfig ~= { _.withESFeatures(_.withUseECMAScript2015(true)) },
    scalacOptions += "-P:scalajs:sjsDefinedByDefault",
  )

lazy val templJVM = templ.jvm
lazy val templJS = templ.js
