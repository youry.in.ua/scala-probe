package server

import scalatags.Text.all._

object Index {
  //val boot =
  //  "console.log('onLoad');"

  val styleMain =   tag("style")("""
      html, body {
        margin: 0;
        padding: 0;
      }"""
  )

  val styleHeader = tag("style")("""
    .header-bar {
      background-color: #353a28;
      width: 100%;
      height: 100px;
    }
    .header-title {
      color:white;
      margin: auto;
      display: block;
      width: 400px;
      font-family: Helvetica;
      font-size: x-large;
      padding-top: 20px
    }"""
  )

  val styleSideBar = tag("style")("""
    :host {
      font-family: "Lato", sans-serif;
    }
    .sidenav {
      height: 100%;
      width: 200px;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: #111;
      overflow-x: hidden;
      padding-top: 20px;
    }
    .sidenav a {
      padding: 6px 6px 6px 32px;
      text-decoration: none;
      font-size: 25px;
      color: #818181;
      display: block;
    }
    .sidenav a:hover {
      color: #f1f1f1;
    }
    .main {
      margin-left: 200px;  Same as the width of the sidenav
    }
    @media screen and (max-height: 450px) {
      .sidenav {padding-top: 15px;}
      .sidenav a {font-size: 18px;}
    }"""
  )

  val styleMainArea = tag("style")("""
    main {
      margin-left: 200px; /* Same as the width of the sidenav */
    }"""
  )

  val styleMainAreaSection = tag("style")("""
    .container {
      display: flex;
      width: 40%;
      flex-direction: column;
      margin: auto;
      padding-top: 15%;
    }
    tr:nth-child(even) {background-color: #f2f2f2;}
    th {
      background-color: #4CAF50;
      color: white;
    }
    .action-button {
      width: 100px;
      margin: auto;
    }
    .data-table {
      border-collapse: collapse
    }"""
  )

  val skeleton =
    html(
      head(
        meta(charset:="utf-8"),
        script(src:="client-fastopt.js"),
        script("document.addEventListener('DOMContentLoaded', function() {Client.main('hello')})"),
      ),
      styleMain,
      body(
        div(id:="hello"),
        tag("app-element")(
          tag("header-bar")(attr("slot"):="header-bar"),
          tag("side-nav")(attr("slot"):="side-nav"),

          tag("main-area")(attr("slot"):="main-area")(
            tag("add-section"),
            tag("list-section"),
            tag("delete-section"),
          ),

        ),
        tag("template")(id:="app-element-template")(
          tag("slot")(name:="header-bar"),
          tag("slot")(name:="side-nav"),
          tag("slot")(name:="main-area"),
        ),
        tag("template")(id:="header-bar-template")(
          styleHeader,
          div(cls:="header-bar")(
             span(cls:="header-title")("Web components in Scala.js"),
          ),
        ),
        tag("template")(id:="side-nav-template")(
          styleSideBar,
          div(cls:="sidenav")(
          ),
        ),
        tag("template")(id:="main-area-template")(
          styleMainArea,
          tag("main")(
            tag("slot")(),
          ),
        ),
        tag("template")(id:="main-area-section-template")(
          styleMainAreaSection,
          tag("section")(
            div(cls:="container")(
            )
          ),
        ),
      )
    )
}
