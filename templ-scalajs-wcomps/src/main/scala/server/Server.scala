package server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import akka.http.scaladsl.model._
import akka.http.scaladsl.Http
//import akka.http.scaladsl.model.HttpMethods._

import akka.http.scaladsl.server.Directives._

import scala.io.StdIn

object Server {

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  val route =
      get {
        path(Segment) { name =>
          //logger.info("get resource: {}", name)
          println(s"get resource: ${name}")
          getFromResource(s"$name") // uses implicit ContentTypeResolver
        } ~
        path("") {
          //logger.info("get : Page.skeleton.render")
          println("get : Page.skeleton.render")
          complete{
            HttpEntity(
              ContentTypes.`text/html(UTF-8)`,
              "<!DOCTYPE html>\n" + Index.skeleton.render
            )
          }
        }
      }
  def main(args: Array[String]): Unit = {

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)

    //logger.info(s"Server online at http://localhost:8080/")

    println("\nPress RETURN to stop...")

    StdIn.readLine()

    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}
